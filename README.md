# Factom Discord Tip Bot
Tip people on Discord using Factoids!

### What is this repository for?

* Tip other users with Factoids `!tip @sam $5`
* Deposit and withdraw Factoids to your user account with `!deposit` and `!withdraw`
* Check your tipping account balance using `!balance`
* Account deposit limit of $50USD worth of Factoids

### Who do I talk to?
* Samuel Vanderwaal (@SamuelVanderwall)
* Adam S Levy (@AdamSLevy)

## Install and Build
We are using Go Modules for golang dependency management. This means you must
use Go 1.11 or higher.

This uses SQLite3 which means that this program links against the sqlite3
library and uses cgo during the build process. You must have `gcc` installed
and have `CGO_ENABLED=1` in your environment when calling `go build`.

### DB migrations
We are using [`goose`](https://github.com/pressly/goose) for migrations.

Migrations can be done using the `db/migrations/migrate` bash script. Modify
the script so that it has the correct database user, password, and table that
you set up for the tipbot. The migrations for mysql assume the database already
exists.

For sqlite, just comment out the `mysql` lines and the script will create a
database file called `tipbot.db`. You can rename and move this file anywhere.
Use the `-dbpath` option to point to the database file. Specifying this option
makes the tipbot use SQLite3 over mysql/mariadb.

### `factom-walletd`
This app uses
[`factom-walletd`](https://github.com/FactomProject/factom-walletd) to manage
deposit wallets and generating and signing transactions. This must be running
before you start the tipbot.

### `factomd` This app must be able to query a
[`factomd`](https://github.com/FactomProject/factomd) node's HTTP API to
monitor the blockchain and broadcast transactions. You can set up your own node
or use `courtesy-node.factom.com`. If you use the courtesy node you may want to
increase the `-factomdtimeout` option.

### Setting up the discord bot
Follow these instructions to set up a bot and get a token to use with this app:
[https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token)

## How does it work?
This tipbot records all tips internally, and only makes transactions onchain
for withdrawals. A unique deposit address is generated for each user that
queries the tipbot. Deposits are detected by regularly scanning the blockchain
for Factoid transactions with outputs that match a deposit address we control.
Withdrawals pull any available funds from any deposit address and fees are
subtracted from the user's balance.

### Packages
The `db/` package includes a goroutine that serializes all database access by
listening on a number of channels dedicated to various db operations.

The `srv/` package interacts with discord to parse and validate any user
commands. Valid commands are sent via channels over to the `db`'s goroutine and
block until the `db` returns a success or failure. User error messages are sent
privately back to the user. Successful tips are posted publically.

The `flag/` package parses all cli flags and environment variables. It is used
by all of the other packages. Command line arguments take precedence over
environment variables.

### Logging
All successful operations that change users' balances are logged both to stdout
but to a dedicated transaction log file. This is useful when using something
like systemd-journald to capture logs since systemd-journald only periodically
flushes logs to disk, so the last few minutes of logs can be lost in the event
of a system crash. The log file will be written to disk much more reliably.

Events and errors are logged to stderr.

If you add the `-debug` flag passwords and secret tokens will get printed to
stdout. Be aware that this may pose a security risk and should never be used in
production.
