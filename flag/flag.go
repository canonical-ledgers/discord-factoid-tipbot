package flag

import (
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"
	"time"

	"github.com/AdamSLevy/factom"
	"github.com/posener/complete"
	log "github.com/sirupsen/logrus"
)

const (
	// Environment variable name prefix
	envNamePrefix = "DISCORD_FACTOID_TIPBOT_"

	// Environment variable names
	dbPathEnvName    = envNamePrefix + "DB_PATH"
	dbUserEnvName    = envNamePrefix + "DB_USER"
	dbPassEnvName    = envNamePrefix + "DB_PASSWORD"
	dbHostEnvName    = envNamePrefix + "DB_HOST"
	dbNameEnvName    = envNamePrefix + "DB_NAME"
	txLogFileEnvName = envNamePrefix + "TX_LOGFILE"

	discordTokenEnvName = envNamePrefix + "TOKEN"

	startScanHeightEnvName = envNamePrefix + "START_SCAN_HEIGHT"

	dailyWithdrawalLimitEnvName = envNamePrefix + "DAILY_WITHDRAWAL_LIMIT"
	dailyTipLimitEnvName        = envNamePrefix + "DAILY_TIP_LIMIT"
	absoluteTipLimitEnvName     = envNamePrefix + "ABSOLUTE_TIP_LIMIT"
	accountBalanceLimitEnvName  = envNamePrefix + "ACCOUNT_BALANCE_LIMIT"

	factomdUserEnvName        = envNamePrefix + "FACTOMD_USER"
	factomdPassEnvName        = envNamePrefix + "FACTOMD_PASSWORD"
	factomdTLSCertFileEnvName = envNamePrefix + "FACTOMD_TLS_CERT"
	factomdTLSEnableEnvName   = envNamePrefix + "FACTOMD_TLS_ENABLE"
	factomdServerEnvName      = envNamePrefix + "FACTOMD_SERVER"
	factomdTimeoutEnvName     = envNamePrefix + "FACTOMD_TIMEOUT"

	walletUserEnvName        = envNamePrefix + "WALLET_USER"
	walletPassEnvName        = envNamePrefix + "WALLET_PASSWORD"
	walletTLSCertFileEnvName = envNamePrefix + "WALLET_TLS_CERT"
	walletTLSEnableEnvName   = envNamePrefix + "WALLET_TLS_ENABLE"
	walletServerEnvName      = envNamePrefix + "WALLET_SERVER"
	walletTimeoutEnvName     = envNamePrefix + "WALLET_TIMEOUT"

	// Defaults
	dbPathDefault    = "./tipbot.sqlite3"
	dbUserDefault    = "tipbot"
	dbHostDefault    = "localhost:3306"
	dbNameDefault    = "tipbot"
	txLogFileDefault = "./txlog"

	dailyWithdrawalLimitDefault = math.MaxUint64
	dailyTipLimitDefault        = math.MaxUint64
	absoluteTipLimitDefault     = math.MaxUint64
	accountBalanceLimitDefault  = math.MaxUint64

	factomdServerDefault  = "localhost:8088"
	factomdTimeoutDefault = 0 * time.Second

	walletServerDefault  = "localhost:8089"
	walletTimeoutDefault = 0 * time.Second
)

var (
	LogDebug     bool
	DiscordToken string
	DBPath       string
	DBUser       string
	DBPass       string
	DBHost       string
	DBName       string
	TxLogFile    string

	DailyWithdrawalLimit uint64
	DailyTipLimit        uint64
	AbsoluteTipLimit     uint64
	AccountBalanceLimit  uint64
	StartScanHeight      uint64

	flagset    map[string]bool
	Completion *complete.Complete
	rpc        = factom.RpcConfig
	flagLog    *log.Entry
)

func init() {
	flag.BoolVar(&LogDebug, "debug", false, "Log debug messages")

	// Database options
	flag.StringVar(&DBPath, "dbpath", dbPathDefault,
		appendEnvName("Path to a database file (sqlite3)", dbPathEnvName))
	flag.StringVar(&DBUser, "dbuser", dbUserDefault,
		appendEnvName("Database username (mysql)", dbUserEnvName))
	flag.StringVar(&DBPass, "dbpass", "",
		appendEnvName("Database password (mysql)", dbPassEnvName))
	flag.StringVar(&DBHost, "dbhost", dbHostDefault,
		appendEnvName("Database hostname (mysql)", dbHostEnvName))
	flag.StringVar(&DBName, "dbname", dbNameDefault,
		appendEnvName("Database name (mysql)", dbNameEnvName))

	flag.StringVar(&TxLogFile, "txlog", txLogFileDefault,
		appendEnvName("Transaction log file", txLogFileEnvName))

	// Discord options
	flag.StringVar(&DiscordToken, "token", "",
		appendEnvName("Discord bot secret token", discordTokenEnvName))

	// Tipbot limits
	flag.Uint64Var(&DailyWithdrawalLimit, "dailywithdrawallimit",
		dailyWithdrawalLimitDefault,
		appendEnvName("Maximum allowed withdrawal amount in factoshis within a "+
			"24 hour period per user",
			dailyWithdrawalLimitEnvName))
	flag.Uint64Var(&DailyTipLimit, "dailytiplimit", dailyTipLimitDefault,
		appendEnvName("Maximum allowed tip amount in factoshis within a 24 hour "+
			"period per user",
			dailyTipLimitEnvName))
	flag.Uint64Var(&AbsoluteTipLimit, "absolutetiplimit", absoluteTipLimitDefault,
		appendEnvName("Maximum allowed tip amount in factoshis per tip",
			absoluteTipLimitEnvName))
	flag.Uint64Var(&AccountBalanceLimit, "accountbalancelimit",
		accountBalanceLimitDefault,
		appendEnvName("Maximum allowed account balance in factoshis before a "+
			"user's account is locked for withdrawals only",
			accountBalanceLimitEnvName))
	flag.Uint64Var(&StartScanHeight, "startscanheight", 0,
		appendEnvName("Block height to start scanning for deposits on startup",
			startScanHeightEnvName))

	// Factomd configuration options
	flag.StringVar(&rpc.FactomdServer, "s", factomdServerDefault,
		appendEnvName("IPAddr:port# of factomd API to use to access blockchain",
			factomdServerEnvName))
	flag.StringVar(&rpc.FactomdRPCUser, "factomduser", "",
		appendEnvName("Username for API connections to factomd", factomdUserEnvName))
	flag.StringVar(&rpc.FactomdRPCPassword, "factomdpassword", "",
		appendEnvName("Password for API connections to factomd", factomdPassEnvName))
	flag.StringVar(&rpc.FactomdTLSCertFile, "factomdcert",
		"~/.factom/m2/factomdAPIpub.cert",
		appendEnvName("This file is the TLS certificate provided by the factomd API",
			factomdTLSCertFileEnvName))
	flag.BoolVar(&rpc.FactomdTLSEnable, "factomdtls", false,
		appendEnvName("Set to true when the factomd API is encrypted",
			factomdTLSEnableEnvName))
	flag.DurationVar(&rpc.FactomdTimeout, "factomdtimeout", factomdTimeoutDefault,
		appendEnvName("Timeout for factomd API requests, 0 means never timeout",
			factomdTimeoutEnvName))

	// Factom-walletd configuration options
	flag.StringVar(&rpc.WalletServer, "w", walletServerDefault,
		appendEnvName("IPAddr:port# of factom-walletd API to use to create "+
			"transactions", walletServerEnvName))
	flag.StringVar(&rpc.WalletRPCUser, "walletuser", "",
		appendEnvName("Username for API connections to factom-walletd",
			walletUserEnvName))
	flag.StringVar(&rpc.WalletRPCPassword, "walletpassword", "",
		appendEnvName("Password for API connections to factom-walletd",
			walletPassEnvName))
	flag.StringVar(&rpc.WalletTLSCertFile, "walletcert",
		"~/.factom/m2/factom-walletdAPIpub.cert",
		appendEnvName("This file is the TLS certificate provided by the "+
			"factom-walletd API", walletTLSCertFileEnvName))
	flag.BoolVar(&rpc.WalletTLSEnable, "wallettls", false,
		appendEnvName("Set to true when the factom-walletd API is encrypted",
			walletTLSEnableEnvName))
	flag.DurationVar(&rpc.WalletTimeout, "wallettimeout", walletTimeoutDefault,
		appendEnvName("Timeout for wallet API requests, 0 means never timeout",
			walletTimeoutEnvName))

	// CLI completion spec
	cmd := complete.Command{
		Flags: complete.Flags{
			"-debug": complete.PredictNothing,

			"-dbpath": complete.PredictFiles("*"),
			"-dbhost": complete.PredictAnything,
			"-dbname": complete.PredictAnything,
			"-dbpass": complete.PredictAnything,
			"-dbuser": complete.PredictAnything,
			"-txlog":  complete.PredictFiles("*"),

			"-token": complete.PredictAnything,

			"-startscanheight": complete.PredictAnything,

			"-dailywithdrawallimit": complete.PredictAnything,
			"-dailytiplimit":        complete.PredictAnything,
			"-absolutetiplimit":     complete.PredictAnything,
			"-accountbalancelimit":  complete.PredictAnything,

			"-s":               complete.PredictAnything,
			"-factomdtimeout":  complete.PredictAnything,
			"-factomduser":     complete.PredictAnything,
			"-factomdpassword": complete.PredictAnything,
			"-factomdcert":     complete.PredictFiles("*"),
			"-factomdtls":      complete.PredictNothing,

			"-w":              complete.PredictAnything,
			"-wallettimeout":  complete.PredictAnything,
			"-walletuser":     complete.PredictAnything,
			"-walletpassword": complete.PredictAnything,
			"-walletcert":     complete.PredictFiles("*"),
			"-wallettls":      complete.PredictNothing,

			"-y":                   complete.PredictNothing,
			"-installcompletion":   complete.PredictNothing,
			"-uninstallcompletion": complete.PredictNothing,
		},
	}

	// Add flags for self installing the CLI completion tool
	Completion = complete.New(os.Args[0], cmd)
	Completion.CLI.InstallName = "installcompletion"
	Completion.CLI.UninstallName = "uninstallcompletion"
	Completion.AddFlags(nil)
}

func Parse() {
	flag.Parse()
	flagset = make(map[string]bool)
	flag.Visit(func(f *flag.Flag) { flagset[f.Name] = true })

	_log := log.New()
	_log.Formatter = &log.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if LogDebug {
		_log.SetLevel(log.DebugLevel)
	}
	flagLog = _log.WithField("pkg", "flag")

	// Load options from environment variables if they haven't been
	// specified on the command line.
	loadFromEnv(&DBPath, "dbpath", dbPathEnvName)
	loadFromEnv(&DBPass, "dbpass", dbPassEnvName)
	loadFromEnv(&DBUser, "dbuser", dbUserEnvName)
	loadFromEnv(&DBName, "dbname", dbNameEnvName)
	loadFromEnv(&DBHost, "dbhost", dbHostEnvName)

	loadFromEnv(&TxLogFile, "txlog", txLogFileEnvName)

	loadFromEnv(&DiscordToken, "token", discordTokenEnvName)

	loadFromEnv(&StartScanHeight, "startscanheight", startScanHeightEnvName)

	loadFromEnv(&DailyWithdrawalLimit, "dailywithdrawallimit", dailyWithdrawalLimitEnvName)
	loadFromEnv(&DailyTipLimit, "dailytiplimit", dailyTipLimitEnvName)
	loadFromEnv(&AbsoluteTipLimit, "absolutetiplimit", absoluteTipLimitEnvName)
	loadFromEnv(&AccountBalanceLimit, "accountbalancelimit", accountBalanceLimitEnvName)

	loadFromEnv(&rpc.FactomdServer, "s", factomdServerEnvName)
	loadFromEnv(&rpc.FactomdRPCUser, "factomduser", factomdUserEnvName)
	loadFromEnv(&rpc.FactomdRPCPassword, "factomdpass", factomdPassEnvName)
	loadFromEnv(&rpc.FactomdTLSCertFile, "factomdcert", factomdTLSCertFileEnvName)
	loadFromEnv(&rpc.FactomdTLSEnable, "factomdtls", factomdTLSEnableEnvName)
	loadFromEnv(&rpc.FactomdTimeout, "factomdtimeout", factomdTimeoutEnvName)

	loadFromEnv(&rpc.WalletServer, "w", walletServerEnvName)
	loadFromEnv(&rpc.WalletRPCUser, "walletuser", walletUserEnvName)
	loadFromEnv(&rpc.WalletRPCPassword, "walletpass", walletPassEnvName)
	loadFromEnv(&rpc.WalletTLSCertFile, "walletcert", walletTLSCertFileEnvName)
	loadFromEnv(&rpc.WalletTLSEnable, "wallettls", walletTLSEnableEnvName)
	loadFromEnv(&rpc.WalletTimeout, "wallettimeout", walletTimeoutEnvName)

}

func Validate() {
	dbPass := ""
	if len(DBPass) == 0 {
		dbPass = "<redacted>"
	}
	discordToken := ""
	if len(DiscordToken) > 0 {
		discordToken = "<redacted>"
	}
	factomdRPCPassword := ""
	if len(rpc.FactomdRPCPassword) > 0 {
		factomdRPCPassword = "<redacted>"
	}
	walletRPCPassword := ""
	if len(rpc.WalletRPCPassword) > 0 {
		walletRPCPassword = "<redacted>"
	}

	flagLog.Debugf("-dbhost %#v", DBHost)
	flagLog.Debugf("-dbname %#v", DBName)
	flagLog.Debugf("-dbpass %v ", dbPass)
	flagLog.Debugf("-dbuser %#v", DBUser)
	debugPrintln()

	flagLog.Debugf("-txlog  %#v", TxLogFile)
	debugPrintln()

	flagLog.Debugf("-token  %v ", discordToken)
	debugPrintln()

	flagLog.Debugf("-startscanheight      %v ", StartScanHeight)
	flagLog.Debugf("-dailywithdrawallimit %v FCT",
		factom.FactoshiToFactoid(DailyWithdrawalLimit))
	flagLog.Debugf("-dailytiplimit        %v FCT",
		factom.FactoshiToFactoid(DailyTipLimit))
	flagLog.Debugf("-absolutetiplimit     %v FCT",
		factom.FactoshiToFactoid(AbsoluteTipLimit))
	flagLog.Debugf("-accountbalancelimit  %v FCT",
		factom.FactoshiToFactoid(AccountBalanceLimit))
	debugPrintln()

	flagLog.Debugf("-s              %#v", rpc.FactomdServer)
	flagLog.Debugf("-factomduser    %#v", rpc.FactomdRPCUser)
	flagLog.Debugf("-factomdpass    %v ", factomdRPCPassword)
	flagLog.Debugf("-factomdcert    %#v", rpc.FactomdTLSCertFile)
	flagLog.Debugf("-factomdtimeout %v ", rpc.FactomdTimeout)
	debugPrintln()

	flagLog.Debugf("-w              %#v", rpc.WalletServer)
	flagLog.Debugf("-walletuser     %#v", rpc.WalletRPCUser)
	flagLog.Debugf("-walletpass     %v ", walletRPCPassword)
	flagLog.Debugf("-walletcert     %#v", rpc.WalletTLSCertFile)
	flagLog.Debugf("-wallettimeout  %v ", rpc.WalletTimeout)

	// Validate options
	if len(DBPath) == 0 {
		if len(DBPass) == 0 {
			flagLog.Warn("No database password specified.")
		}
	} else {
		if len(DBPass) > 0 {
			flagLog.Warn("Database password may not be used with " +
				"SQLite3 database.")
		}
	}

	if len(DiscordToken) == 0 {
		flagLog.Fatalf("No discord token specified.")
	}

}

func loadFromEnv(v interface{}, flagName, envName string) {
	if flagset[flagName] {
		return
	}
	envVar, ok := os.LookupEnv(envName)
	if len(envVar) > 0 {
		switch v := v.(type) {
		case *string:
			*v = envVar
		case *time.Duration:
			duration, err := time.ParseDuration(envVar)
			if err != nil {
				flagLog.Fatalf("Environment Variable %v: "+
					"time.ParseDuration(\"%v\"): %v",
					envName, envVar, err)
			}
			*v = duration
		case *uint64:
			val, err := strconv.ParseUint(envVar, 10, 64)
			if err != nil {
				flagLog.Fatalf("Environment Variable %v: "+
					"strconv.ParseUint(\"%v\", 10, 64): %v",
					envName, envVar, err)
			}
			*v = val
		case *bool:
			if ok {
				*v = true
			}
		}
	}
}

func appendEnvName(msg, envName string) string {
	return fmt.Sprintf("%s\nEnvironment variable: %v", msg, envName)
}

func debugPrintln() {
	if LogDebug {
		fmt.Println()
	}
}
