-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE metadata
(
  id            SMALLINT UNSIGNED PRIMARY KEY DEFAULT 0,
  fblock_height BIGINT DEFAULT 0,
  total_balance BIGINT UNSIGNED DEFAULT 0
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE metadata;
