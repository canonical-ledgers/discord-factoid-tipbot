-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE users
(
  discord_id         VARCHAR(32) NOT NULL PRIMARY KEY,
  deposit_address VARCHAR(52) NOT NULL UNIQUE,
  account_balance BIGINT UNSIGNED DEFAULT 0
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE users;
