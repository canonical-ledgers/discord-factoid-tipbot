-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE transactions
(
  id            VARCHAR(32) NOT NULL PRIMARY KEY,
  datetime      DATETIME NOT NULL,
  kind          VARCHAR(10) NOT NULL,
  from_id       VARCHAR(52) NOT NULL,
  to_id         VARCHAR(52) NOT NULL,
  tx_id         VARCHAR(64) NOT NULL,
  fblock_height BIGINT,
  amount        BIGINT UNSIGNED NOT NULL,
  CONSTRAINT unique_deposits UNIQUE (tx_id, to_id)
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE transactions;
