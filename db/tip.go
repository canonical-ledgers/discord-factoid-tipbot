package db

import (
	"errors"
	"fmt"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"
	"github.com/AdamSLevy/factom"
	"github.com/gocraft/dbr"
)

// Tip wraps transaction struct for tips.
type Tip struct {
	transaction
	response chan txResponse
}

func (t *Tip) Send() error {
	t.response = make(chan txResponse)
	tips <- *t
	res := <-t.response
	t.transaction = res.transaction
	return res.err
}

const tipKind transactionKind = "tip"

// NewTip returns a new Tip struct with given values.
func NewTip(fromID, toID string, amount uint64) *Tip {
	return &Tip{transaction: transaction{
		Kind:   tipKind,
		FromID: fromID,
		ToID:   toID,
		Amount: amount,
	}}
}

const BalanceTooHigh = `
Your account is locked due a balance that exceeds %v FCT. This tipbot is not
intended to store large amounts of funds. You must withdraw at least %v FCT
before you will be permitted to tip or deposit.
`

func doTip(t *Tip) (error, error) {
	// Validate tip amount
	if t.Amount == 0 {
		return errors.New("Cannot tip 0."), nil
	}
	if t.Amount > flag.AbsoluteTipLimit {
		return fmt.Errorf("You may not tip more than %v FCT at a time.",
			factom.FactoshiToFactoid(flag.AbsoluteTipLimit)), nil
	}
	if t.FromID == t.ToID {
		return errors.New("You cannot tip yourself."), nil
	}

	// Load from user data.
	from := User{ID: t.FromID}
	var err error
	if err := from.readOrCreate(sess); err != nil {
		return nil, fmt.Errorf("User%+v.readOrCreate(): %v", from, err)
	}

	// Verify sufficient funds
	if t.Amount > from.AccountBalance {
		return errors.New("Insufficient balance."), nil
	}

	// Prohibit tipping when the user's balance is too high
	if from.AccountBalance > flag.AccountBalanceLimit {
		return fmt.Errorf(BalanceTooHigh,
			factom.FactoshiToFactoid(flag.AccountBalanceLimit),
			factom.FactoshiToFactoid(
				from.AccountBalance-flag.AccountBalanceLimit),
		), nil
	}

	// Prohibit tips that exceed the daily tip limit.
	amountPastDay, err := from.txAmountPastDay(tipKind, sess)
	if err != nil {
		return nil, fmt.Errorf("User%+v.txAmountPastDay(%v): %v",
			tipKind, from, err)
	}
	if amountPastDay+t.Amount > flag.DailyTipLimit {
		var amountRemaining uint64
		if amountPastDay < flag.DailyTipLimit {
			amountRemaining = flag.DailyTipLimit - amountPastDay
		}
		return fmt.Errorf(
			"You may not tip more than %v FCT in a 24 hour period. "+
				"You may currently tip up to %v FCT.",
			factom.FactoshiToFactoid(flag.DailyTipLimit),
			factom.FactoshiToFactoid(amountRemaining),
		), nil
	}

	// Load to user data.
	to := User{ID: t.ToID}
	if err := to.readOrCreate(sess); err != nil {
		return nil, fmt.Errorf("User%+v.readOrCreate(): %v", to, err)
	}

	// Edit AccountBalances
	from.AccountBalance -= t.Amount
	to.AccountBalance += t.Amount

	// Start DB transaction.
	var dbTx *dbr.Tx
	dbTx, err = sess.Begin()
	if err != nil {
		return nil, fmt.Errorf("dbr.Session%+v.Begin(): %v", sess, err)
	}
	defer dbTx.RollbackUnlessCommitted()

	// Create tip transaction.
	if err = t.create(dbTx); err != nil {
		return nil, fmt.Errorf("Tip%+v.create(): %v", t, err)
	}

	// Update both Users.
	if err = from.update(dbTx); err != nil {
		return nil, fmt.Errorf("User%+v.update(): %v", from, err)
	}
	if err = to.update(dbTx); err != nil {
		return nil, fmt.Errorf("User%+v.update(): %v", to, err)
	}

	return nil, dbTx.Commit()
}
