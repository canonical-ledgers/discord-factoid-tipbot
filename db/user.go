package db

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/AdamSLevy/factom"
	"github.com/gocraft/dbr"
)

// User struct for database entries
type User struct {
	ID             string `db:"discord_id"`
	DepositAddress string
	AccountBalance uint64
	readRes        chan error
}

// Read sends the User to the db for reading.
func (u *User) Read() error {
	u.readRes = make(chan error)
	defer close(u.readRes)
	users <- u
	return <-u.readRes
}

// Name of users table
var usersTbl = "users"

// Indexes to the user. For use in a WHERE clause. Matches either the primary
// key ID or the unique key ID).
func (u User) index() dbr.Builder {
	return dbr.Or(
		dbr.Eq("discord_id", u.ID),
		dbr.Eq("deposit_address", u.DepositAddress),
	)
}

// create() a new User in the users table. The User object must have a ID.
// Users are uniquely indexed by ID so the given User must not already exist in
// the table. A new secure randomly generated deposit wallet keypair is
// generated for the new user. The DepositAddress fields of the User must be
// empty.
func (u *User) create(sess dbr.SessionRunner) error {
	if len(u.DepositAddress) != 0 {
		return errors.New("DepositAddress not empty")
	}
	adr, err := factom.GenerateFactoidAddress()
	if err != nil {
		return fmt.Errorf("factom.GenerateFactoidAddress(): %v", err)
	}
	u.DepositAddress = adr.String()
	var res sql.Result
	res, err = sess.InsertInto(usersTbl).Columns(
		"discord_id",
		"deposit_address",
	).Record(u).Exec()
	if err != nil {
		return fmt.Errorf("sess.InsertInto(%#v).Columns(%#v, %#v)."+
			"Record(%+v).Exec(): %v",
			usersTbl, "discord_id", "deposit_address", u, err)
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("res.RowsAffected(): %v", err)
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

// read() a User's data into memory from the database. The User must have
// either a valid ID primary key set or a valid ID. The User must already exist
// in the users table. After calling read(), if the returned error is nil, the
// User's fields will be populated.
func (u *User) read(sess dbr.SessionRunner) error {
	return sess.Select("discord_id, deposit_address, account_balance").
		From(usersTbl).
		Where(u.index()).
		LoadStruct(u)
}

// readOrCreate() a User if it does not already exist.
func (u *User) readOrCreate(sess dbr.SessionRunner) error {
	if err := u.read(sess); err != nil {
		return u.create(sess)
	}
	return nil
}

// update() a User's AccountBalance and AddressBalance in the users table.
func (u User) update(sess dbr.SessionRunner) error {
	res, err := sess.Update(usersTbl).
		Set("account_balance", u.AccountBalance).
		Where(u.index()).Exec()
	if err != nil {
		return fmt.Errorf("sess.Update(%#v).Set(%#v, %#v).Where(...).Exec(): %v",
			usersTbl, "account_balance", u.AccountBalance, err)
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("res.RowsAffected(): %v", err)
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

func (u User) transactions(sess dbr.SessionRunner) ([]Transaction, error) {
	var txs []Transaction
	n, err := sess.Select("*").
		From(transactionsTbl).
		Where(dbr.Or(
			dbr.Eq("from_id", u.ID),
			dbr.Eq("from_id", u.DepositAddress),
			dbr.Eq("to_id", u.ID),
			dbr.Eq("to_id", u.DepositAddress),
		)).Load(&txs)
	if err != nil {
		return nil, fmt.Errorf("sess.Select(%#v).From(%#v)."+
			"Where(...).Load(&txs): %v", "*",
			transactionsTbl, err)
	}
	return txs[:n], nil
}

func (u User) txAmountPastDay(kind transactionKind, sess dbr.SessionRunner) (uint64, error) {
	var amount uint64
	_, err := sess.Select("IFNULL(SUM(amount),0)").
		From(transactionsTbl).
		Where(dbr.And(
			dbr.Eq("from_id", u.ID),
			dbr.Eq("kind", kind),
			dbr.Gt("datetime", time.Now().Add(-24*time.Hour)),
		)).Load(&amount)
	if err != nil {
		return 0, fmt.Errorf("sess.Select(%#v).From(%#v).Where(...)."+
			"Load(&amount): %v",
			"IFNULL(SUM(amount),0)", transactionsTbl, err)
	}
	dbLog.Debugf("User%+v.txAmountPastDay(%#v): %v", u, kind, amount)
	return amount, nil
}
