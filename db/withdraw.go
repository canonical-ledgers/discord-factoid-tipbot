package db

import (
	"errors"
	"fmt"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"
	"github.com/AdamSLevy/factom"
	"github.com/gocraft/dbr"
)

// Withdrawal wraps transaction struct for withdrawals.
type Withdrawal struct {
	transaction
	all      bool
	response chan txResponse
}

func (w *Withdrawal) Send() error {
	w.response = make(chan txResponse)
	withdrawals <- *w
	res := <-w.response
	w.transaction = res.transaction
	return res.err
}

const withdrawalKind transactionKind = "withdrawal"

// NewWithdrawal returns a new Withrawal struct with given values.
func NewWithdrawal(userID, toAddress string, amount uint64) *Withdrawal {
	return &Withdrawal{transaction: transaction{
		Kind:   withdrawalKind,
		FromID: userID,
		ToID:   toAddress,
		Amount: amount,
	}}
}

// NewWithdrawAll returns a new Withdrawal struct with "all" boolean set to true.
func NewWithdrawAll(userID, toAddress string) *Withdrawal {
	return &Withdrawal{transaction: transaction{
		Kind:   "withdrawal",
		FromID: userID,
		ToID:   toAddress,
	}, all: true,
	}
}

func doWithdraw(w *Withdrawal) (error, error) {
	// Load user data.
	from := User{ID: w.FromID}
	if err := from.readOrCreate(sess); err != nil {
		return nil, fmt.Errorf("User%+v.readOrCreate(): %v", from, err)
	}

	// Withdraw all.
	if w.all {
		w.Amount = from.AccountBalance
	}

	// Validate withdrawal amount.
	if w.Amount > from.AccountBalance {
		return errors.New("Insufficient balance"), nil
	}
	if w.Amount == 0 {
		return errors.New("You may not withdraw 0"), nil
	}

	// Prohibit withdrawals to any deposit address.
	u := &User{DepositAddress: w.ToID}
	if err := u.read(sess); err == nil {
		return errors.New("You may not withdraw to a user's deposit address."), nil
	}

	// Prohibit withdrawals that exceed the daily withdrawal limit.
	amountPastDay, err := from.txAmountPastDay(withdrawalKind, sess)
	if err != nil {
		return nil, fmt.Errorf("User%+v.txAmountPastDay(%v): %v",
			withdrawalKind, from, err)
	}
	if amountPastDay+w.Amount > flag.DailyWithdrawalLimit {
		var amountRemaining uint64
		if amountPastDay < flag.DailyTipLimit {
			amountRemaining = flag.DailyTipLimit-amountPastDay
		}
		return fmt.Errorf(
			"You may not withdraw more than %v FCT in a 24 hour period. "+
				"You may currently withdraw up to %v FCT.",
			factom.FactoshiToFactoid(flag.DailyWithdrawalLimit),
			factom.FactoshiToFactoid(amountRemaining),
		), nil
	}

	// Adjust balances.
	from.AccountBalance -= w.Amount
	metadata.TotalBalance -= int64(w.Amount)

	// Start DB transaction.
	var dbTx *dbr.Tx
	dbTx, err = sess.Begin()
	if err != nil {
		return nil, fmt.Errorf("sess.Begin(): %v", err)
	}
	defer dbTx.RollbackUnlessCommitted()

	// Create withdrawal. This generates a uuid used as the Name for the
	// factoid tx in factom-walletd.
	if err := w.create(dbTx); err != nil {
		return nil, fmt.Errorf("Withdrawal%+v.create(): %v", w, err)
	}

	// Prepare the factoid withdrawal transaction with factom-walletd.
	if err := createTransaction(w); err != nil {
		return nil, fmt.Errorf("createTransaction(Withdrawal%+v): %v", w, err)
	}
	// Broadcast the factoid withdrawal transaction using factom-walletd.
	if _, err := factom.SendTransaction(w.ID); err != nil {
		return nil, fmt.Errorf("factom.SendTransaction(%v): %v", w.ID, err)
	}

	// Update withdrawal with the new factoid txid.
	if err := w.update(dbTx); err != nil {
		return nil, fmt.Errorf("Withdrawal%+v.update(): %v", w, err)
	}
	// Update user.
	if err := from.update(dbTx); err != nil {
		return nil, fmt.Errorf("User%+v.update(): %v", from, err)
	}
	// Update metadata.
	if err := metadata.update(dbTx); err != nil {
		return nil, fmt.Errorf("metadata%+v.update(): %v", metadata, err)
	}

	return nil, dbTx.Commit()
}
