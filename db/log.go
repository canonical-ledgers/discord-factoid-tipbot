package db

import (
	"fmt"
	"io"
	"os"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"

	log "github.com/sirupsen/logrus"
)

var (
	txLog  *log.Logger
	_dbLog *log.Logger
	dbLog  *log.Entry
)

var txLogFile *os.File

func setupLoggers() error {
	txLog = log.New()
	txLog.Out = os.Stdout
	_dbLog = log.New()
	_dbLog.Formatter = &log.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if flag.LogDebug {
		txLog.SetLevel(log.DebugLevel)
		_dbLog.SetLevel(log.DebugLevel)
	}
	dbLog = _dbLog.WithField("pkg", "db")

	if len(flag.TxLogFile) > 0 {
		var err error
		txLogFile, err = os.OpenFile(flag.TxLogFile,
			os.O_WRONLY|os.O_CREATE|os.O_APPEND,
			0640)
		if err != nil {
			return fmt.Errorf("os.OpenFile(%v): %v", flag.TxLogFile, err)
		}
		txLog.Out = io.MultiWriter(txLogFile, os.Stdout)
	}

	return nil
}

func closeLogFile() {
	if txLogFile != nil {
		dbLog.Debugf("Closing Tx Log File: %v", flag.TxLogFile)
		if err := txLogFile.Close(); err != nil {
			dbLog.Errorf("txLogFile.Close(): %v", err)
		}
	}
}
