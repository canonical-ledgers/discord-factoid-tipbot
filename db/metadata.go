package db

import (
	"database/sql"
	"fmt"

	"github.com/gocraft/dbr"
)

// User struct for database entries
type metadata_t struct {
	ID           uint64 `db:"id"`
	FBlockHeight int64  `db:"fblock_height"`
	TotalBalance int64
}

// Name of users table
var metadataTbl = "metadata"

func (m metadata_t) index() dbr.Builder {
	return dbr.Eq("id", m.ID)
}

func (m *metadata_t) create(sess dbr.SessionRunner) error {
	var res sql.Result
	res, err := sess.InsertInto(metadataTbl).Columns(
		"id",
		"fblock_height",
		"total_balance",
	).Record(m).Exec()
	if err != nil {
		return err
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

func (m *metadata_t) read(sess dbr.SessionRunner) error {
	return sess.Select("fblock_height, total_balance").
		From(metadataTbl).
		Where(m.index()).
		LoadStruct(m)
}

// readOrCreate() a User if it does not already exist.
func (m *metadata_t) readOrCreate(sess dbr.SessionRunner) error {
	if err := m.read(sess); err != nil {
		return m.create(sess)
	}
	return nil
}

// update() a User's AccountBalance and AddressBalance in the users table.
func (m metadata_t) update(sess dbr.SessionRunner) error {
	res, err := sess.Update(metadataTbl).
		Set("total_balance", m.TotalBalance).
		Set("fblock_height", m.FBlockHeight).
		Where(m.index()).Exec()
	if err != nil {
		return err
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}
