package db

import (
	"testing"

	"github.com/gocraft/dbr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// destroy() a User from the users table. This is destructive. The users data
// will be destroyed after this call returns successfully.
func (u User) destroy(sess dbr.SessionRunner) {
	sess.DeleteFrom(usersTbl).
		Where(u.index()).Exec()
	sess.DeleteFrom(transactionsTbl).
		Where(dbr.Or(
			dbr.Eq("to_id", u.ID),
			dbr.Eq("from_id", u.ID),
		)).Exec()
}

func TestUser(t *testing.T) {
	// Setup
	assert := assert.New(t)
	require := require.New(t)
	dbSess := newDBSession()
	dbTx, err := dbSess.Begin()
	require.NoError(err, "sess.Begin()")
	defer dbTx.Rollback()
	uID := "Utest"
	uID2 := "Utest2"
	testUserInit := User{ID: uID}
	user := testUserInit
	user2 := testUserInit
	user2.ID = uID2

	// Create a user with a non empty DepositAddress should fail
	user.DepositAddress = "not empty"
	assert.Error(user.create(dbTx), "User.create() with non-empty DepositAddress")
	user.DepositAddress = ""

	// Create a valid user should succeed
	require.NoError(user.create(dbTx), "User.create() -- first")
	// User.DepositAddress should be length 52
	assert.Len(user.DepositAddress, 52, "user.DepositAddress")

	// Create a second valid user
	require.NoError(user2.create(dbTx), "User.create() -- second")

	// Create a duplicate User should fail
	userDup := testUserInit
	assert.Error(userDup.create(dbTx), "User.create() -- duplicate User.ID")

	// Read invalid User should fail
	userCp := User{}
	require.Error(userCp.read(dbTx), "User.read() -- empty User.ID")

	// Read non existant User should fail
	userCp = User{ID: "NONEXISTENT"}
	require.Error(userCp.read(dbTx), "User.read() -- User.ID not in table")

	// Read User by ID
	userCp = testUserInit
	require.NoError(userCp.read(dbTx), "User.read() -- lookup by User.ID")
	assert.Equal(user, userCp, "User.read() -- lookup by User.ID")

	// Read User by DepositAddress
	userCp = User{DepositAddress: userCp.DepositAddress}
	require.NoError(userCp.read(dbTx), "User.read() -- lookup by User.DepositAddress")
	assert.Equal(user, userCp, "User.read() -- lookup by User.DepositAddress")

	// Update with positive AccountBalance should succeed
	user.AccountBalance = uint64(5123)
	userCp = user
	// Set DepositAddress to empty to test that the column does not get updated
	userCp.DepositAddress = ""
	require.NoError(userCp.update(dbTx), "User.update() -- lookup by User.ID")
	// Read back the user to refresh the DepositAddress that we set empty above
	require.NoError(userCp.read(dbTx), "User.read() -- after User.update()")
	assert.Equal(user, userCp, "User.update() -- User.AccountBalance")

	user.AccountBalance = uint64(500)
	userCp = user
	// Set ID to empty to test that the column does not get updated
	userCp.ID = ""
	require.NoError(userCp.update(dbTx), "User.update() -- lookup by User.DepositAddress")
	// Read back the user to refresh the ID that we set empty above
	require.NoError(userCp.read(dbTx), "User.read() -- after User.update()")
	assert.Equal(user, userCp, "User.update() -- User.AccountBalance")

	// Destroy a valid user should succeed and only affect one row
	user.destroy(dbTx)
}
