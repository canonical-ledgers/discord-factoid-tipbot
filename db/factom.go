package db

import (
	"encoding/json"
	"fmt"

	"github.com/AdamSLevy/factom"
)

func getFCTTransactionsByHeight(height int64) ([]FCTTransaction, error) {
	block, err := factom.GetFBlockByHeight(height)
	if err != nil {
		return nil, fmt.Errorf("factom.GetHeights(%v): %v", height, err)
	}
	jsonBytes, err := json.Marshal(block.FBlock)
	if err != nil {
		return nil, fmt.Errorf("json.Marshal() %v", err)
	}
	var fblock FBlock
	err = json.Unmarshal(jsonBytes, &fblock)
	if err != nil {
		return nil, fmt.Errorf("json.Unmarshal() %v", err)
	}

	return fblock.Transactions, nil
}

type FBlock struct {
	Transactions []FCTTransaction
}

type FCTTransaction struct {
	TxID    string `json:"txid"`
	Outputs []Output
}

type Output struct {
	Amount  uint64
	Address string `json:"useraddress"`
}

func createTransaction(w *Withdrawal) error {
	// Create a new factoid transaction in factom-walletd.
	if _, err := factom.NewTransaction(w.ID); err != nil {
		return fmt.Errorf("factom.NewTransaction(%v): %v", w.ID, err)
	}

	// Add the user's public address as the output.
	if _, err :=
		factom.AddTransactionOutput(w.ID, w.ToID, uint64(w.Amount)); err != nil {
		return fmt.Errorf("factom.AddTransactionOutput(%v, %v, %v): %v",
			w.ID, w.ToID, uint64(w.Amount), err)
	}

	var users []User
	remainingAmount := w.Amount
	// Loop through all users with pagination. Check each user's account
	// balance. Add any available balance as an input to the transaction
	// until remainingAmount is zero.
	for page := uint64(0); remainingAmount > 0 &&
		(len(users) > 0 || page == 0); page++ {
		_, err := sess.Select(
			"deposit_address").
			From(usersTbl).
			Paginate(page, pageSize).
			LoadStructs(&users)
		if err != nil {
			return fmt.Errorf("sess.Select(\"deposit_address\")"+
				".From(\"%v\").Paginate(%v, %v).LoadStructs(): %v",
				usersTbl, page, pageSize, err)
		}

		// Add tx inputs until remainingAmount is 0
		for _, u := range users {
			// Get addressBalance.
			aB, err := factom.GetFactoidBalance(u.DepositAddress)
			if err != nil {
				return fmt.Errorf("factom.GetFactoidBalance(%v): %v",
					u.DepositAddress, err)
			}
			addressBalance := uint64(aB)
			if addressBalance >= remainingAmount {
				// Add entire remaining amount if this address has enough.
				_, err := factom.AddTransactionInput(w.ID,
					u.DepositAddress,
					remainingAmount)
				if err != nil {
					return fmt.Errorf("factom.AddTransactionInput("+
						"%v, %v, %v): %v", w.ID, u.DepositAddress,
						remainingAmount, err)
				}
				// Now transaction inputs == transaction outputs.
				remainingAmount = 0 // Exit the outer loop.
				break               // Exit current loop.
			} else if addressBalance < remainingAmount {
				// Otherwise use everything stored at this address.
				_, err := factom.AddTransactionInput(w.ID,
					u.DepositAddress,
					addressBalance)
				if err != nil {
					return fmt.Errorf("factom.AddTransactionInput("+
						"%v, %v, %v): %v", w.ID, u.DepositAddress,
						addressBalance, err)
				}
				remainingAmount -= addressBalance
				// Repeat until remainingAmount is 0.
			}
		}
	}
	// Require remainingAmount be zero.
	if remainingAmount > 0 {
		return fmt.Errorf(
			"remainingAmount (%v) is greater than 0 after parsing all addresses",
			remainingAmount)
	}

	// Subtract the transaction fee from the output.
	if _, err := factom.SubTransactionFee(w.ID, w.ToID); err != nil {
		return fmt.Errorf("factom.SubTransactionFee(%v, %v): %v",
			w.ID, w.ToID, err)
	}

	// Sign the transaction, generating a factoid txid.
	fctTx, err := factom.SignTransaction(w.ID, false)
	if err != nil {
		return fmt.Errorf("factom.SignTransaction(%v, false): %v", w.ID, err)
	}
	w.TxID = fctTx.TxID // Save the factoid txid to the Withdrawal.

	// The factoid transaction is now built, signed, and ready to be sent
	// by factom-walletd.
	return nil
}
