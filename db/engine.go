package db

import (
	"fmt"
	"time"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"
	"github.com/AdamSLevy/factom"
	"github.com/gocraft/dbr"
)

var (
	sess        *dbr.Session
	tips        chan Tip
	users       chan *User
	Deposits    chan *DepositUser
	withdrawals chan Withdrawal
	errorExit   chan error
	cleanStop   chan error
	metadata    metadata_t
)

// Start() opens the database connection, sets up channels, and launches the
// listen() goroutine. Start returns the channel that will receive errors from
// the listen() goroutine.
func Start() (chan error, error) {
	// Set up the loggers
	if err := setupLoggers(); err != nil {
		return nil, fmt.Errorf("setupLoggers(): %v", err)
	}

	var err error
	if len(flag.DBPath) > 0 {
		dbLog.Debug("Using SQLite3 database")
		sess, err = newSQLiteDBSession()
	} else {
		dbLog.Debug("Using MySQL database")
		sess, err = newMySQLDBSession()
	}
	if err != nil {
		return nil, fmt.Errorf("newDBSession(): %v", err)
	}
	dbLog.Debug("DB connection established.")

	// Load the application metadata
	if err := metadata.readOrCreate(sess); err != nil {
		return nil, fmt.Errorf("metadata.readOrCreate(): %v", err)
	}

	// Get current block height.
	heights, err := factom.GetHeights()
	if err != nil {
		return nil, fmt.Errorf("factom.GetHeights(): %v", err)
	}
	// If FBlockHeight has not been initialized then we just start from the
	// current block.
	if metadata.FBlockHeight == 0 {
		dbLog.Debugf("metadata%+v.FBlockHeight is uninitialized.", metadata)
		metadata.FBlockHeight = heights.EntryHeight
		dbLog.Debugf("Setting metadata.FBlockHeight = %v", heights.LeaderHeight-1)
	}

	if flag.StartScanHeight != 0 {
		if int64(flag.StartScanHeight) > heights.EntryHeight {
			return nil, fmt.Errorf("StartScanHeight (%v) is larger "+
				"than current EntryHeight (%v).",
				flag.StartScanHeight,
				heights.EntryHeight)
		}
		dbLog.Debugf("Overriding metadata.FBlockHeight (%v) with "+
			"StartScanHeight (%v).",
			metadata.FBlockHeight,
			flag.StartScanHeight)
		metadata.FBlockHeight = int64(flag.StartScanHeight)
	}

	errorExit = make(chan error)
	cleanStop = make(chan error)
	tips = make(chan Tip)
	users = make(chan *User)
	withdrawals = make(chan Withdrawal)
	Deposits = make(chan *DepositUser)

	// Scan latest blocks for new deposits.
	dbLog.Infof("Scanning for deposits since last start up...")
	err = scanNewBlocksForDeposits()
	if err != nil {
		return nil, fmt.Errorf("scanNewBlocksForDeposits(): %v", err)
	}

	if err = dbSanityCheck(); err != nil {
		return nil, fmt.Errorf("dbSanityCheck(): %v", err)
	}

	// Launch the channel listener thread.
	go listen()

	return errorExit, nil
}

// Stop() sends a stop signal to the listen() goroutine and then closes the
// database session.
func Stop() {
	cleanStop <- nil
	closeAll()
}

// errorStop() sends an error back to main() from the listen() goroutine.
func errorStop(err error) {
	dbLog.Error(err)
	errorExit <- err
	closeAll()
}

func closeAll() {
	if err := sess.Close(); err != nil {
		dbLog.Errorf("sess.Close(): %v", err)
	}
	closeLogFile()
	close(errorExit)
	close(cleanStop)
	close(tips)
	close(users)
	close(withdrawals)
	close(Deposits)
}

var blockTime = 1 * time.Minute
var pageSize = uint64(200)

// This is the database engine runtime loop. This is the only goroutine that
// accesses the database to avoid any race conditions. It serializes tips and
// withdrawals sent over channels from the srv package. Deposits are detected
// by scanning each new block's transactions for outputs that match a user's
// deposit address. Any errors that are passed back to this function are sent
// back to main() over the errorExit channel. Main sends a nil error over the
// cleanStop channel when the program is shutting down.
func listen() {
	dbLog.Debugf("Started db.listen() goroutine")
	blockTick := time.Tick(blockTime)
	for {
		select {
		case t := <-tips:
			userErr, err := doTip(&t)
			if err != nil {
				errorStop(fmt.Errorf("doTip(Tip%+v): %v", t, err))
				return
			}
			if userErr != nil {
				dbLog.Debugf("Failed Tip%+v: %v", t, userErr)
			} else {
				txLog.Infof("Tip%+v", t)
			}
			t.response <- txResponse{transaction: t.transaction, err: userErr}
		case w := <-withdrawals:
			userErr, err := doWithdraw(&w)
			if err != nil {
				errorStop(fmt.Errorf("doWithdraw(Withdrawal%+v): %v",
					w, err))
				return
			}
			if userErr != nil {
				dbLog.Debugf("Failed Withdrawal%+v: %v", w, userErr)
			} else {
				txLog.Infof("Withdrawal%+v", w)
			}
			w.response <- txResponse{transaction: w.transaction, err: userErr}
		case u := <-users:
			err := u.readOrCreate(sess)
			u.readRes <- err
			if err != nil {
				errorStop(fmt.Errorf("User%+v.readOrCreate(): %v", u, err))
				return
			}
		case <-blockTick:
			err := scanNewBlocksForDeposits()
			if err != nil {
				errorStop(fmt.Errorf("scanNewBlocksForDeposits(): %v", err))
				return
			}
			if err = dbSanityCheck(); err != nil {
				errorStop(fmt.Errorf("dbSanityCheck(): %v", err))
				return
			}
		case <-cleanStop:
			dbLog.Debug("Clean stop signal received.")
			return
		}
	}
}

func scanNewBlocksForDeposits() error {
	// Get the current leader's block height
	heights, err := factom.GetHeights()
	if err != nil {
		return fmt.Errorf("factom.GetHeights(): %v", err)
	}
	currentHeight := heights.EntryHeight
	// Scan blocks from the last saved FBlockHeight up to but not including
	// the leader height
	for height := metadata.FBlockHeight; height < currentHeight; height++ {
		dbLog.Debugf("Scanning block %v for deposits.", height)
		// Get the transactions from this block
		fctTransactions, err := getFCTTransactionsByHeight(height)
		if err != nil {
			return fmt.Errorf("getFCTTransactionsByHeight(%v): %v",
				height, err)
		}
		// Scan the block's FCT transactions for deposits
		err = scanFCTTransactionsForDeposits(height, fctTransactions)
		if err != nil {
			return fmt.Errorf("scanFCTTransactionsForDeposits(%v): %v",
				height, err)
		}
		metadata.FBlockHeight = height + 1
		if err := metadata.update(sess); err != nil {
			return fmt.Errorf("metadata%+v.update(): %v", metadata, err)
		}
	}

	return nil
}

func scanFCTTransactionsForDeposits(height int64, fctTxs []FCTTransaction) error {
	// For each factoid transaction...
	for _, ftx := range fctTxs {
		// For each output in a factoid transaction...
		for _, o := range ftx.Outputs {
			// See if any user exists with this deposit address.
			u := User{DepositAddress: o.Address}
			err := u.read(sess)
			if err != nil {
				// No user with this deposit address.
				continue // Check next output address.
			}

			// Check if we have already tracked this deposit.
			d := NewDeposit(u, ftx.TxID, o.Amount, height)
			if err := d.read(sess); err == nil {
				dbLog.Debugf("Already saved Deposit%+v", d)
				// We already recorded this transaction.
				continue // Check next transaction.
			}

			// Save the deposit.
			err = doDeposit(&u, &d)
			if err != nil {
				return fmt.Errorf("doDeposit(User%+v, Deposit%+v): %v",
					u, d, err)
			}
			txLog.Infof("Deposit%+v", d)
			dbLog.Debugf("TotalBalance: %v Factoshi", metadata.TotalBalance)

			// Send a signal to srv package to notify the user
			// privately of the deposit.
			notifyDeposit(d, u)
		}
	}
	return nil
}

// dbSanityCheck checks the sum of account balances and on-chain FCT balances
// of deposit addresses for all users to guarantee that the tipbot is fully
// funded, and that account balances haven't been tampered with.
func dbSanityCheck() error {
	pageSize := uint64(100)
	var fctAddressTotal, userAccountTotal uint64
	dbLog.Debug("Running db sanity check...")
	var n int
	// Paginate through all users.
	for page := uint64(1); n > 0 || page == 1; page++ {
		var users []User
		var err error
		n, err = sess.Select(
			"deposit_address, account_balance").
			From(usersTbl).
			Paginate(page, pageSize).
			LoadStructs(&users)
		if err != nil {
			return fmt.Errorf("sess.Select(%#v).From(%#v)."+
				"Paginate(%v, %v).LoadStructs(&users): %v",
				"deposit_address, account_balance", usersTbl,
				page, pageSize, err)
		}
		// For each user's deposit address, add the on chain FCT
		// balance to the fctAddressTotal. Also sum the recorded
		// account balances. Note that account balances and deposit
		// address on chain balances do not necessarily match.
		for _, u := range users {
			userAccountTotal += u.AccountBalance
			balance, err := factom.GetFactoidBalance(u.DepositAddress)
			if err != nil {
				return fmt.Errorf("factom.GetFactoidBalance(%#v): %v",
					u.DepositAddress, err)
			}
			fctAddressTotal += uint64(balance)
		}
	}

	// Load the application metadata.
	if err := metadata.readOrCreate(sess); err != nil {
		return fmt.Errorf("metadata.readOrCreate(): %v", err)
	}

	var err error
	// The metadata.TotalBalance is computed with each deposit and
	// withdrawal and so should match the actual sum of all the balances.
	if metadata.TotalBalance != int64(userAccountTotal) {
		err = fmt.Errorf("metadata.TotalBalance (%v) != userAccountTotal (%v)",
			metadata.TotalBalance, userAccountTotal)
		dbLog.Error(err)
	}

	// The sum of the on chain balances of the deposit addresses may exceed
	// the user account total if there have been withdrawals that have yet
	// to be processed or had failed at one point.
	if fctAddressTotal < userAccountTotal {
		err = fmt.Errorf("fctAddressTotal (%v) < userAccountTotal (%v)",
			fctAddressTotal, userAccountTotal)
		dbLog.Error(err)
	}

	return err
}
