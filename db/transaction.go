package db

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/gocraft/dbr"
	"github.com/satori/go.uuid"
)

// Name of users table
var transactionsTbl = "transactions"

type Transaction interface {
	Send() error
	GetToID() string
	GetAmount() uint64
	GetTxID() string
}

type txResponse struct {
	err error
	transaction
}

type transactionKind string

// transaction is the base struct for database transactions.
type transaction struct {
	ID           string
	Datetime     time.Time
	Kind         transactionKind
	FromID       string
	ToID         string
	Amount       uint64
	TxID         string
	FBlockHeight int64 `db:"fblock_height"`
}

func (t transaction) GetToID() string {
	return t.ToID
}

func (t transaction) GetAmount() uint64 {
	return t.Amount
}

func (t transaction) GetTxID() string {
	return t.TxID
}

// transaction creates an entry into the transactions table of the database.
func (t *transaction) create(sess dbr.SessionRunner) error {
	if len(t.ID) != 0 {
		return errors.New("ID not empty")
	}
	id, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("uuid.NewV4(): %v", err)
	}
	// We use this ID as the Name for the transaction for factom-walletd,
	// which enforces a max line length of 32. The uuid returns a 32 byte
	// id but it has hyphens in it which makes it exceed the length. So we
	// remove all '-' characters.
	t.ID = strings.Replace(id.String(), "-", "", -1)
	// Use ID as a placeholder for TxID to satisfy its UNIQUE constraint
	if len(t.TxID) == 0 {
		t.TxID = t.ID
	}
	t.Datetime = time.Now()
	_, err = sess.InsertInto(transactionsTbl).Columns(
		"id",
		"datetime",
		"kind",
		"from_id",
		"to_id",
		"tx_id",
		"amount",
	).Record(t).Exec()
	return err
}

// transaction creates an entry into the transactions table of the database.
func (t *transaction) update(sess dbr.SessionRunner) error {
	if len(t.ID) == 0 {
		return errors.New("ID is empty")
	}
	res, err := sess.Update(transactionsTbl).
		Set("tx_id", t.TxID).
		Where(dbr.Eq("id", t.ID)).Exec()
	if err != nil {
		return fmt.Errorf("sess.Update(%#v).Set(%#v, %#v)."+
			"Where(dbr.Eq(%#v, %#v)).Exec(): %v)",
			transactionsTbl, "tx_id", t.TxID, "id", t.ID, err)
	}
	var n int64
	n, err = res.RowsAffected()
	if err != nil {
		return fmt.Errorf("res.RowsAffected(): %v", err)
	}
	if n != 1 {
		return fmt.Errorf("Invalid number of rows affected: %v", n)
	}
	return nil
}

func (t transaction) index() dbr.Builder {
	return dbr.Or(
		dbr.And(
			dbr.Eq("tx_id", t.TxID),
			dbr.Eq("to_id", t.ToID),
		),
		dbr.Eq("id", t.ID),
	)
}

func (t *transaction) read(sess dbr.SessionRunner) error {
	return sess.Select(
		"id",
		"datetime",
		"from_id",
		"to_id",
		"tx_id",
		"amount",
	).From(transactionsTbl).
		Where(t.index()).
		LoadStruct(t)
}
