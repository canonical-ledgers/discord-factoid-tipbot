package db

import (
	"fmt"

	"github.com/gocraft/dbr"
)

// Deposit wraps the transaction struct for deposits.
type Deposit struct {
	transaction
}

const depositKind transactionKind = "deposit"

// NewDeposit returns a new Deposit struct with given values.
func NewDeposit(user User, txID string, amount uint64, fBlockHeight int64) Deposit {
	return Deposit{transaction: transaction{
		Kind:         depositKind,
		ToID:         user.DepositAddress,
		FromID:       user.ID,
		Amount:       amount,
		TxID:         txID,
		FBlockHeight: fBlockHeight,
	}}
}

type DepositUser struct {
	Deposit
	User
}

func notifyDeposit(d Deposit, u User) {
	go func() { Deposits <- &DepositUser{Deposit: d, User: u} }()
}

func doDeposit(u *User, d *Deposit) error {
	// Validate deposit
	if d.Amount == 0 {
		return fmt.Errorf("Cannot deposit 0")
	}
	if len(d.TxID) == 0 {
		return fmt.Errorf("No TxID")
	}

	// Update balances.
	u.AccountBalance += d.Amount
	metadata.TotalBalance += int64(d.Amount)

	// Start DB transaction.
	var tx *dbr.Tx
	tx, err := sess.Begin()
	if err != nil {
		return fmt.Errorf("sess.Begin(): %v", err)
	}
	defer tx.RollbackUnlessCommitted()

	// Save Deposit transaction.
	if err := d.create(tx); err != nil {
		return fmt.Errorf("Deposit%+v.create(): %v", d, err)
	}
	// Update user account.
	if err := u.update(tx); err != nil {
		return fmt.Errorf("User%+v.update(): %v", u, err)
	}
	// Update metadata.
	if err := metadata.update(tx); err != nil {
		return fmt.Errorf("metadata%+v.update(): %v", metadata, err)
	}

	return tx.Commit()
}
