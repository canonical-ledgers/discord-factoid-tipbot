package db

import (
	"sync"
	"testing"

	"github.com/gocraft/dbr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var tx *dbr.Tx

var total = uint64(200000)
var tipAmount = uint64(45130)

var u1Init = User{ID: "@U1"}
var u2Init = User{ID: "@U2"}

var u1 = u1Init
var u2 = u2Init

func resetDB(t *testing.T, sess dbr.SessionRunner) {
	u1.destroy(sess)
	u2.destroy(sess)
	u1 = u1Init
	u2 = u2Init
	resetBalances(t, sess)
}

func resetBalances(t *testing.T, sess dbr.SessionRunner) {
	require := require.New(t)
	require.NoError(u1.readOrCreate(sess), "u1.readOrCreate()")
	require.NoError(u2.readOrCreate(sess), "u2.readOrCreate()")
	if u1.AccountBalance != total {
		u1.AccountBalance = total
		u1.AddressBalance = total / 2
		require.NoError(u1.update(sess), "u1.update()")
	}
	if u2.AccountBalance != 0 {
		u2.AccountBalance = 0
		u2.AddressBalance = total / 2
		require.NoError(u2.update(sess), "u2.update()")
	}
}

func TestMain(m *testing.M) {
	// Create two users with balances
	sess := newDBSession()
	Start()
	defer Stop()
	m.Run()
	u1.destroy(sess)
	u2.destroy(sess)
}

func TestTip(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	sess := newDBSession()
	resetDB(t, sess)

	t1 := NewTip(u1.ID, u2.ID, tipAmount)
	for i := 1; i <= 4; i++ {
		require.NoError(t1.Tip(), "t1.Tip()")
		require.NoError(u1.read(sess), "u1.read()")
		require.NoError(u2.read(sess), "u2.read()")
		assert.Equal(total-uint64(i)*tipAmount, u1.AccountBalance, "from user amount")
		assert.Equal(uint64(i)*tipAmount, u2.AccountBalance, "to user amount")
	}
	balance1 := u1.AccountBalance
	balance2 := u2.AccountBalance
	require.Error(t1.Tip(), "t1.Tip()")
	require.NoError(u1.read(sess), "u1.read()")
	require.NoError(u2.read(sess), "u2.read()")
	assert.Equal(balance1, u1.AccountBalance, "from user amount")
	assert.Equal(balance2, u2.AccountBalance, "to user amount")
	assert.Equal(total, u1.AccountBalance+u2.AccountBalance, "total")

	resetBalances(t, sess)
	var wg sync.WaitGroup
	wg.Add(4)
	t.Run("concurrent", func(t *testing.T) {
		for i := 1; i <= 4; i++ {
			t.Run("tip", func(t *testing.T) {
				defer wg.Done()
				t.Parallel()
				require.NoError(t1.Tip(), "t1.Tip()")
			})
		}
	})
	wg.Wait()
	require.Error(t1.Tip(), "t1.Tip()")
	require.NoError(u1.read(sess), "u1.read()")
	require.NoError(u2.read(sess), "u2.read()")
	assert.Equal(total, u1.AccountBalance+u2.AccountBalance, "total")
	var txs1, txs2 []Transaction
	var err error
	txs1, err = u1.transactions(sess)
	require.NoError(err, "u1.transactions()")
	txs2, err = u2.transactions(sess)
	require.NoError(err, "u2.transactions()")
	assert.Len(txs1, 8, "u1 transactions")
	assert.EqualValues(txs1, txs2, "u1 and u2 transactions")

}

func TestWithdraw(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	sess := newDBSession()
	resetDB(t, sess)

	txs1, err := u1.transactions(sess)
	require.NoError(err, "u1.transactions()")
	assert.Len(txs1, 0, "u1 transactions")

	t1 := NewWithdrawal(u1.ID,
		"FA3sYVvrYkxG2SGfNzgyx5KsCvHgKq9ovA1RS2pH1Tuzz7SiS5nH", tipAmount)
	for i := 1; i <= 4; i++ {
		txID, err := t1.Withdraw()
		require.NoError(err, "t1.Withdraw() err")
		assert.NotEmpty(txID, "t1.Withdraw() txID")
		require.NoError(u1.read(sess), "u1.read()")
		assert.Equal(total-uint64(i)*tipAmount, u1.AccountBalance, "from user amount")
	}
	txID, err := t1.Withdraw()
	require.Error(err, "t1.Withdraw() err")
	assert.Empty(txID, "t1.Withdraw() txID")
	require.NoError(u1.read(sess), "u1.read()")
	assert.Equal(total-uint64(4)*tipAmount, u1.AccountBalance, "from user amount")

	txs1, err = u1.transactions(sess)
	require.NoError(err, "u1.transactions()")
	assert.Len(txs1, 4, "u1 transactions")

	resetBalances(t, sess)
	t2 := NewWithdrawAll(u1.ID,
		"FA3sYVvrYkxG2SGfNzgyx5KsCvHgKq9ovA1RS2pH1Tuzz7SiS5nH")
	txID, err = t2.Withdraw()
	require.NoError(err, "t2.Withdraw() err")
	assert.NotEmpty(txID, "t2.Withdraw() txID")
	require.NoError(u1.read(sess), "u1.read()")
	assert.Equal(uint64(0), u1.AccountBalance, "from user amount")

	txID, err = t2.Withdraw()
	require.Error(err, "t2.Withdraw() err")
	assert.Empty(txID, "t2.Withdraw() txID")
	require.NoError(u1.read(sess), "u1.read()")
	assert.Equal(uint64(0), u1.AccountBalance, "from user amount")
}
