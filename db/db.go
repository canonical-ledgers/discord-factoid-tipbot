package db

import (
	"fmt"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gocraft/dbr"
	_ "github.com/mattn/go-sqlite3"
)

func newMySQLDBSession() (*dbr.Session, error) {
	dbString := flag.DBUser + ":" + flag.DBPass + "@tcp(" + flag.DBHost + ")/" +
		flag.DBName + "?parseTime=true"
	conn, err := dbr.Open("mysql", dbString, nil)
	if err != nil {
		return nil, fmt.Errorf("dbr.Open(\"mysql\", \"%v\", nil): %v",
			dbString, err)
	}
	return conn.NewSession(nil), nil
}

func newSQLiteDBSession() (*dbr.Session, error) {
	conn, err := dbr.Open("sqlite3", flag.DBPath, nil)
	if err != nil {
		return nil, fmt.Errorf("dbr.Open(\"sqlite3\", \"%v\", nil): %v",
			flag.DBPath, err)
	}
	return conn.NewSession(nil), nil
}
