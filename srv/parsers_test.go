package srv

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var archaeopteryx = "<@307230753551548417>"

func TestParseTip(t *testing.T) {
	tables := []struct {
		text string
		err  error
	}{
		{"!tip <@307230753551548417> 10", nil},
		{"!tip <@307230753551548417> $10", nil},
		{"!tip <@307230753551548417> €10", nil},
		{"!tip <@307230753551548417> 10 FCT", nil},
		{"!tip <@307230753551548417> 10 USD", nil},
		{"!tip <@307230753551548417> 10 EUR", nil},
		{"!tip <@307230753551548417> 1.4321231", nil},
		{"!tip <@307230753551548417> $1.4321231", nil},
		{"!tip <@413284736293748372> 0.0321", nil},
		{"", errors.New("Invalid number of arguments")},
		{"<@307230753551548417>", errors.New("Invalid number of arguments")},
		{"!tip <@307230753551548417>", errors.New("Invalid number of arguments")},
		{"!tip <@307230753551548417> 2 USD FCT", errors.New("Invalid number of arguments")},
		{"!tip @sam#217 10", errors.New("Invalid recipient")},
		{"!tip <@307230753551548417> $2 USD", errors.New("Invalid amount")},
		{"<!tip <@307230753551548417> 10,100", errors.New("Invalid amount")},
		{"<!tip <@413284736293748372> four", errors.New("Invalid amount")},
		{"<!tip <@307230753551548417><@413284736293748372> 10", errors.New("You can only tip one user at a time")},
	}

	for _, table := range tables {
		_, err := parseTip(table.text)
		assert.Equal(t, table.err, err)
	}
}

func TestParseWithdrawal(t *testing.T) {
	tables := []struct {
		text string
		err  error
	}{
		{"!withdraw all FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"!withdraw 10 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"!withdraw 1.3145 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"!withdraw $10 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"!withdraw €10 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"!withdraw 10 USD FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", nil},
		{"", errors.New("Invalid number of arguments")},
		{"!withdraw", errors.New("Invalid number of arguments")},
		{"!withdraw 10", errors.New("Invalid number of arguments")},
		{"!withdraw 10 EUR FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9 another", errors.New("Invalid number of arguments")},
		{"!withdraw $10 USD FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", errors.New("Invalid amount")},
		{"!withdraw alk FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", errors.New("Invalid amount")},
		{"!withdraw 1,414 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", errors.New("Invalid amount")},
		{"!withdraw 12 FX2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng9", errors.New("Invalid Factoid address")},
		{"!withdraw 12 FA2BCCVgPD7ZGFpYpzbWXbqtmJroL6wQFcJgPSME6coUqh9EUng", errors.New("Invalid Factoid address")},
	}

	for _, table := range tables {
		_, err := parseWithdrawal(table.text)
		assert.Equal(t, table.err, err)
	}
}
