package srv

import (
	"fmt"
	"strings"

	"github.com/AdamSLevy/factom"
	"github.com/abrander/coinmarketcap"
	"github.com/bwmarrin/discordgo"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/db"
	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"
)

var (
	dgSess    *discordgo.Session
	errorExit chan error
	cmcClient *coinmarketcap.Client
)

const (
	cmdPrefix      = "!testnet-"
	depositCmd     = cmdPrefix + "deposit"
	withdrawCmd    = cmdPrefix + "withdraw"
	balanceCmd     = cmdPrefix + "balance"
	tipCmd         = cmdPrefix + "tip"
	tipbotCmd      = cmdPrefix + "tipbot"
	disclaimerText = `
DISCLAIMER: *This tipbot is running on the Factom Community Testnet. It does
NOT work with real Factoids.* **All balances and amounts represented by this
tipbot have ZERO TRADEABLE VALUE!**

`
	depositWarningText = `
WARNING: *Do NOT attempt to send real factoids to the tipbot.* **You will lose
funds!**

`
	withdrawalWarningText = `
WARNING: *This withdrawal has occurred on the Factom Community Testnet. The
transaction will NOT show up on any blockchain explorers for Factom mainnet.*

`
)

// Start() starts DiscordGo session, opens websocket and listens for events.
func Start(token string) (chan error, error) {
	setupLoggers()

	// Connect to CoinMarketCap for price data.
	var err error
	cmcClient, err = coinmarketcap.NewClient()
	if err != nil {
		return nil, fmt.Errorf("coinmarketcap.NewClient(): %v", err)
	}

	// Create a new DiscordGo Session.
	dgSess, err = discordgo.New("Bot " + token)
	if err != nil {
		return nil, fmt.Errorf("discordgo.New(\"Bot%v\"): %v", token, err)
	}

	// Register the handler for new messages.
	dgSess.AddHandler(messageCreateHandler)

	// Open websocket connection to Discord and start listening.
	if err = dgSess.Open(); err != nil {
		return nil, fmt.Errorf("discordgo.Session.Open(): %v", err)
	}

	srvLog.Debug("Opened discord websocket.")

	errorExit = make(chan error)
	go depositListen()
	return errorExit, nil
}

func Stop() {
	closeAll()
}

func errorStop(err error) {
	srvLog.Error(err)
	errorExit <- err
	closeAll()
}

func closeAll() {
	err := dgSess.Close()
	if err != nil {
		srvLog.Errorf("discordgo.Session.Close(): %v", err)
	}
	srvLog.Debug("Closed Discord websocket.")
	err = cmcClient.Close()
	if err != nil {
		srvLog.Errorf("cmcClient.Close(): %v", err)
	}
	srvLog.Debug("Closed CoinMarketCap client.")
	close(errorExit)
}

func depositListen() {
	for {
		select {
		case depositUser := <-db.Deposits:
			if depositUser == nil {
				srvLog.Debug("srv.depositListen() exit")
				return
			}
			userMsg := disclaimerText +
				fmt.Sprintf("New deposit of %v! Your new balance is %v FCT",
					factoshiToFactoid(depositUser.GetAmount()),
					factoshiToFactoid(depositUser.AccountBalance))
			sendPrivateMessage(dgSess, depositUser.ID, userMsg)

			// Notify the user when the user's balance is too high
			if depositUser.AccountBalance > flag.AccountBalanceLimit {
				sendPrivateMessage(dgSess, depositUser.ID,
					fmt.Sprintf(db.BalanceTooHigh,
						factom.FactoshiToFactoid(
							flag.AccountBalanceLimit),
						factom.FactoshiToFactoid(
							depositUser.AccountBalance-
								flag.AccountBalanceLimit)))
			}
		}
	}
}

func messageCreateHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Don't respond to Bot's own messages
	if m.Author.ID == s.State.User.ID {
		return
	}

	// Ignore empty messages
	if len(m.Content) == 0 {
		return
	}

	// Split message on spaces
	fields := strings.Fields(m.Content)

	tx, userErr, err := parseTipOrWithdrawal(fields, m.Author.ID)
	if err != nil {
		errorStop(fmt.Errorf("parseTipOrWithdrawal(%v, %v): %v", fields, m.Author.ID, err))
		return
	}
	if userErr != nil {
		sendPrivateMessage(s, m.Author.ID, userErr.Error())
		return
	}
	if tx != nil {
		userErr := tx.Send()
		if userErr != nil {
			// Create a private user channel.
			sendPrivateMessage(s, m.Author.ID, userErr.Error())
			return
		}
		switch tx := tx.(type) {
		case *db.Tip:
			// Get recipient user
			recipient, err := s.User(tx.GetToID())
			if err != nil {
				errorStop(fmt.Errorf("discordgo.Session.User(%v): %v",
					tx.ToID, err))
				return
			}
			// Prepare public success message.
			userMsg := fmt.Sprintf("%v tipped %v %v FCT!",
				m.Author.Mention(), recipient.Mention(),
				factoshiToFactoid(tx.Amount))
			sendPublicMessage(s, m.ChannelID, userMsg)
			// Warn recipient if their account balance is too high.
			u := &db.User{ID: tx.GetToID()}
			err = u.Read()
			if err != nil {
				errorStop(fmt.Errorf("User%+v.Read(): %v", u, err))
				return
			}
			if u.AccountBalance > flag.AccountBalanceLimit {
				sendPrivateMessage(dgSess, u.ID, fmt.Sprintf(db.BalanceTooHigh,
					factom.FactoshiToFactoid(flag.AccountBalanceLimit),
					factom.FactoshiToFactoid(
						u.AccountBalance-flag.AccountBalanceLimit),
				))
			}
			return
		case *db.Withdrawal:
			// Notify the user privately that the tx succeeded.
			userMsg := disclaimerText + withdrawalWarningText +
				fmt.Sprintf("You withdrew %v FCT to %v. Transaction ID: %v",
					factoshiToFactoid(tx.GetAmount()),
					tx.GetToID(), tx.GetTxID())
			sendPrivateMessage(s, m.Author.ID, userMsg)
			return
		default:
			// This should never happen!
			errorStop(fmt.Errorf(
				"Transaction type switch: default case should be unreachable!",
			))
			return
		}
	}

	switch fields[0] {
	case depositCmd:
		u := &db.User{ID: m.Author.ID}
		err := u.Read()
		if err != nil {
			errorStop(fmt.Errorf("User%+v.Read(): %v", u, err))
			return
		}
		// Notify the user when the user's balance is too high
		if u.AccountBalance > flag.AccountBalanceLimit {
			sendPrivateMessage(dgSess, u.ID, fmt.Sprintf(db.BalanceTooHigh,
				factom.FactoshiToFactoid(flag.AccountBalanceLimit),
				factom.FactoshiToFactoid(
					u.AccountBalance-flag.AccountBalanceLimit),
			))
			return
		}
		userMsg := disclaimerText + depositWarningText +
			fmt.Sprintf("Your deposit address: %v", u.DepositAddress)
		sendPrivateMessage(s, m.Author.ID, userMsg)
		return
	case balanceCmd:
		u := &db.User{ID: m.Author.ID}
		err := u.Read()
		if err != nil {
			errorStop(fmt.Errorf("User%+v.Read(): %v", u, err))
			return
		}
		userMsg := disclaimerText +
			fmt.Sprintf("Your current balance: %v FCT",
				factoshiToFactoid(u.AccountBalance))
		sendPrivateMessage(s, m.Author.ID, userMsg)
		return
	case tipbotCmd:
		userMsg := disclaimerText + depositWarningText + `
NOTE: To help ensure that users know that this tipbot is running on the
testnet, all commands start with ` + "`!testnet-`" + `

Commands:
` + "`!testnet-tip @user <amount> <unit>`" + `
>> Send a tip to another user
e.g.: ` + "`!testnet-tip @archaeopteryx#7615 0.213 fct`" + `

` + "`!testnet-withdraw <testoid address> <amount|all>`" + `
>> Withdraw testoids to the provided ` + "`<testoid address>`." + `
e.g.: ` + "`!testnet-withdraw FA31Tsx25VmFki9WHnChSmveqNswuuMy3PMwDPeb1vqUdjTuEaho 0.23`" + `
e.g.: ` + "`!testnet-withdraw FA31Tsx25VmFki9WHnChSmveqNswuuMy3PMwDPeb1vqUdjTuEaho all`" + `

` + "`!testnet-balance`" + `
>> Returns your balance in testoids

` + "`!testnet-deposit`" + `
>> Returns a Testoid deposit address. **DO NOT SEND REAL FACTOIDS TO THIS ADDRESS!**
`
		sendPublicMessage(s, m.ChannelID, userMsg)
		return
	}
}

func sendPrivateMessage(s *discordgo.Session, userID, msg string) {
	// Create a private user channel.
	userCh, err := s.UserChannelCreate(userID)
	if err != nil {
		errorStop(fmt.Errorf("discordgo.Session.UserChannelCreate(%v): %v",
			userID, err))
		return
	}
	// Send the user a private message.
	if _, err = s.ChannelMessageSend(userCh.ID, msg); err != nil {
		errorStop(fmt.Errorf("discordgo.Session.ChannelMessageSend(%v, '%v'): %v",
			userCh.ID, msg, err))
	}
	return
}

func sendPublicMessage(s *discordgo.Session, channelID, msg string) {
	// Send public success message
	if _, err := s.ChannelMessageSend(channelID, msg); err != nil {
		errorStop(fmt.Errorf("discordgo.Session.ChannelMessageSend(%v, '%v'): %v",
			channelID, msg, err))
	}
}

const factoshisPerFactoid = 1E8

func factoshiToFactoid(f uint64) float64 {
	return float64(f) / factoshisPerFactoid
}

func factoidToFactoshi(f float64) uint64 {
	return uint64(round(f * factoshisPerFactoid))
}
