package srv

import (
	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"

	log "github.com/sirupsen/logrus"
)

var (
	_srvLog *log.Logger
	srvLog  *log.Entry
)

func setupLoggers() {
	_srvLog = log.New()
	_srvLog.Formatter = &log.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if flag.LogDebug {
		_srvLog.SetLevel(log.DebugLevel)
	}
	srvLog = _srvLog.WithField("pkg", "srv")
}
