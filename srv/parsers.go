package srv

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/db"
	"github.com/AdamSLevy/factom"
	"github.com/abrander/coinmarketcap"
)

// Parse Discord user IDs
// Matches <@1234>
var (
	matchUserStr     = `\<@([0-9]+)\>`
	matchUser        = regexp.MustCompile(matchUserStr)
	matchAmountStr   = `^\s*\d*\.?\d+\s*$`
	matchAmount      = regexp.MustCompile(matchAmountStr)
	matchCurrencyStr = `([$]|USD|FCT)` // Accepted currency denominations
	matchCurrency    = regexp.MustCompile(matchCurrencyStr)
	currencySymbols  = map[string]string{"$": "USD", "€": "EUR", "£": "GBP"}
	matchAddressStr  = `FA[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50}`
	matchAddress     = regexp.MustCompile(matchAddressStr)
)

// round negative and positive floats
func round(a float64) float64 {
	if a < 0 {
		return math.Ceil(a - 0.5)
	}
	return math.Floor(a + 0.5)
}

// toFactoshis receives a currency and amount and converts it to Factoshis
// by looking up the current price of Factoids on Coinmarketcap.com
func toFactoshis(currency string, amount float64) (uint64, error) {
	result, err := cmcClient.Ticker(coinmarketcap.Currency("factom"))
	if err != nil {
		return 0, fmt.Errorf("cmcClient.Ticker(): %v", err)
	}
	coin := result.Index(0)
	price, err := coin.Price(currency)
	if err != nil {
		return 0, fmt.Errorf("coin.Price(%v): %v", currency, err)
	}
	return factoidToFactoshi(amount / price), nil
}

func parseTipOrWithdrawal(fields []string, fromID string) (db.Transaction, error, error) {
	switch fields[0] {
	case tipCmd:
		return parseTip(fields[1:], fromID)
	case withdrawCmd:
		return parseWithdrawal(fields[1:], fromID)
	}
	return nil, nil, nil
}

func parseTip(fields []string, fromID string) (db.Transaction, error, error) {
	invalidSyntaxError := errors.New("Invalid !tip command syntax. Type !tipbot for help.")
	// Validate command length.
	if len(fields) != 2 && len(fields) != 3 {
		return nil, invalidSyntaxError, nil
	}

	// Parse recipient user id.
	userMatches := matchUser.FindAllStringSubmatch(fields[0], -1)
	if len(userMatches) != 1 || len(userMatches[0]) != 2 || len(userMatches[0][1]) == 0 {
		return nil, invalidSyntaxError, nil
	}
	userID := userMatches[0][1]

	factoshis, userErr, err := parseAmount(fields[1:])
	if userErr != nil || err != nil {
		return nil, userErr, err
	}

	return db.NewTip(fromID, userID, factoshis), nil, nil
}

func parseWithdrawal(fields []string, userID string) (db.Transaction, error, error) {
	invalidSyntaxError := errors.New("Invalid !withdraw command syntax. Type !tipbot for help.")
	if len(fields) != 2 && len(fields) != 3 {
		return nil, invalidSyntaxError, nil
	}

	// Parse withdrawal address.
	addressMatches := matchAddress.FindAllString(fields[0], -1)
	if len(addressMatches) != 1 || factom.IsValidAddress(addressMatches[0]) {
		return nil, errors.New("Invalid !withdraw address."), nil
	}
	address := addressMatches[0]

	if fields[1] == "all" {
		return db.NewWithdrawAll(userID, address), nil, nil
	}

	factoshis, userErr, err := parseAmount(fields[1:])
	if userErr != nil || err != nil {
		return nil, userErr, err
	}

	return db.NewWithdrawal(userID, address, factoshis), nil, nil
}

func parseAmount(fields []string) (uint64, error, error) {
	// Concatenate remaining fields to construct tip amount string.
	tipAmountStr := strings.ToUpper(strings.Join(fields, " "))
	// Check for commas.
	if strings.Contains(tipAmountStr, ",") {
		return 0, errors.New("Invalid amount. Commas are prohibited."), nil
	}
	//srvLog.Debugf("tipAmountStr: '%v'", tipAmountStr)

	// Parse currency denomination.
	currencyIndex := matchCurrency.FindAllStringIndex(tipAmountStr, -1)
	if len(currencyIndex) != 1 {
		return 0, errors.New("Invalid currency denomination."), nil
	}
	currencySymbol := tipAmountStr[currencyIndex[0][0]:currencyIndex[0][1]]
	//srvLog.Debugf("currencySymbol: %v", currencySymbol)

	// Remove the currency denomination.
	tipAmountStr = tipAmountStr[:currencyIndex[0][0]] +
		tipAmountStr[currencyIndex[0][1]:]
	// Parse tip amount.
	amountMatches := matchAmount.FindAllString(tipAmountStr, -1)
	if len(amountMatches) != 1 {
		return 0, errors.New("Invalid amount."), nil
	}
	amount, err := strconv.ParseFloat(strings.TrimSpace(amountMatches[0]), 64)
	if err != nil {
		return 0, errors.New("Invalid amount."), nil
	}
	//srvLog.Debugf("amount: %v", amount)

	// Convert tip amount into factoshis.
	var factoshis uint64
	if currencySymbol == "FCT" {
		factoshis = factoidToFactoshi(amount)
	} else {
		// Convert character symbols to three letter symbol.
		symbol, ok := currencySymbols[currencySymbol]
		if ok {
			currencySymbol = symbol
		}
		factoshis, err = toFactoshis(currencySymbol, amount)
		if err != nil {
			return 0, nil, fmt.Errorf("toFactoshis(%v, %v): %v",
				currencySymbol, amount, err)
		}
	}
	return factoshis, nil, nil
}
