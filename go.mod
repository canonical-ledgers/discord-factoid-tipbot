module bitbucket.org/canonical-ledgers/discord-factoid-tipbot

require (
	github.com/AdamSLevy/factom v0.0.0-20180830002119-fd7faf0ad29d
	github.com/FactomProject/factom v0.3.5 // indirect
	github.com/abrander/coinmarketcap v0.0.0-20180117222920-a6f79d052be2
	github.com/bwmarrin/discordgo v0.18.0
	github.com/go-sql-driver/mysql v1.4.0
	github.com/gocraft/dbr v0.0.0-20180507214907-a0fd650918f6
	github.com/gorilla/websocket v1.3.0 // indirect
	github.com/hashicorp/errwrap v0.0.0-20180715044906-d6c0cd880357 // indirect
	github.com/hashicorp/go-multierror v0.0.0-20180717150148-3d5d8f294aa0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/posener/complete v1.1.2
	github.com/satori/go.uuid v0.0.0-20180103174451-36e9d2ebbde5
	github.com/sirupsen/logrus v0.0.0-20180817012529-fc587f31c804
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20180820150726-614d502a4dac // indirect
	golang.org/x/sys v0.0.0-20180823144017-11551d06cbcc // indirect
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
)
