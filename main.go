package main

import (
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/db"
	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/flag"
	"bitbucket.org/canonical-ledgers/discord-factoid-tipbot/srv"
)

func main() { os.Exit(_main()) }

func _main() int {
	flag.Parse()
	// Attempt to run the completion program.
	if flag.Completion.Complete() {
		// The completion program ran, so just return.
		return 0
	}
	flag.Validate()

	// Set up local logger
	_log := log.New()
	_log.Formatter = &log.TextFormatter{ForceColors: true,
		DisableTimestamp:       true,
		DisableLevelTruncation: true}
	if flag.LogDebug {
		_log.SetLevel(log.DebugLevel)
	}
	log := _log.WithField("pkg", "main")

	// Start the db engine.
	dbErrorExit, err := db.Start()
	if err != nil {
		log.Fatalf("db.Start(): %v", err)
	}

	// Start the discord server connection.
	srvErrorExit, err := srv.Start(flag.DiscordToken)
	if err != nil {
		db.Stop()
		log.Fatalf("srv.Start(flag.DiscordToken): %v", err)
	}

	log.Info("Tipbot started.")
	defer log.Info("Tipbot stopped.")

	// Set up interrupts channel.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	// Block until we get an interrupt or an error from srv or db. When we
	// do, stop db and srv and exit gracefully.
	ret := 0
	select {
	case <-sig:
		log.Infof("SIGINT: Shutting down now.")
		db.Stop()
		srv.Stop()
	case <-dbErrorExit:
		srv.Stop()
	case <-srvErrorExit:
		db.Stop()
	}

	return ret
}
